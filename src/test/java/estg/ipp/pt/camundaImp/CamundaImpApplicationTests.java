package estg.ipp.pt.camundaImp;

import static io.camunda.zeebe.process.test.assertions.BpmnAssert.assertThat;
import static io.camunda.zeebe.protocol.Protocol.USER_TASK_JOB_TYPE;
import static io.camunda.zeebe.spring.test.ZeebeTestThreadSupport.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.never;

import io.camunda.zeebe.client.ZeebeClient;
import io.camunda.zeebe.client.api.response.ActivatedJob;
import io.camunda.zeebe.client.api.response.ProcessInstanceEvent;
import io.camunda.zeebe.process.test.api.ZeebeTestEngine;
import io.camunda.zeebe.spring.test.ZeebeSpringTest;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeoutException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import estg.ipp.pt.camundaImp.Main.CamundaImpApplication;
import estg.ipp.pt.camundaImp.Model.ClientRegistrationModel;
import estg.ipp.pt.camundaImp.Service.ClientRegistrationService;

@SpringBootTest(classes = CamundaImpApplication.class)
@ZeebeSpringTest
class CamundaImpApplicationTests {
	/** 
	@Autowired
	private ZeebeClient client;

	@Autowired
	private ZeebeTestEngine testEngine;

	@MockBean
	private ClientRegistrationService clientRegistrationService;

	public void waitForUserTaskAndComplete(String userTaskId, Map<String, Object> variables)
			throws InterruptedException, TimeoutException {

		// Let the workflow engine do whatever it needs to do
		testEngine.waitForIdleState(Duration.ofMinutes(5));

		// Now get all user tasks
		List<ActivatedJob> jobs = client
				.newActivateJobsCommand()
				.jobType(USER_TASK_JOB_TYPE)
				.maxJobsToActivate(1)
				.workerName("waitForUserTaskAndComplete")
				.send()
				.join()
				.getJobs();

		// Should be only one
		assertTrue(jobs.size() > 0, "Job for user task '" + userTaskId + "' does not exist");
		ActivatedJob userTaskJob = jobs.get(0);

		// Make sure it is the right one
		if (userTaskId != null) {
			assertEquals(userTaskId, userTaskJob.getElementId());
		}

		// And complete it passing the variables
		if (variables != null && variables.size() > 0) {
			client.newCompleteCommand(userTaskJob.getKey()).variables(variables).send().join();
		} else {
			client.newCompleteCommand(userTaskJob.getKey()).send().join();
		}
	}

	@Test
	public void testClientRegisteredApproved() throws Exception {
		ClientRegistrationModel variables = new ClientRegistrationModel(
				"Joao", // name
				"123456789", // contact
				"123456789", // nif
				"Portugal", // country
				"Street 123", // address
				"1234-567", // postalCode
				"joao@example.com", // email
				"password123", // password
				"EUR" // preferredCurrency
		);

		ProcessInstanceEvent processInstance = client.newCreateInstanceCommand()
				.bpmnProcessId("ClientRegistrationProcess")
				.latestVersion().variables(variables).send().join();

		waitForUserTaskAndComplete(
				"Activity_1cxsesc", Collections.singletonMap("approved", true));

		waitForUserTaskAndComplete(
				"Activity_0n4s7o6", Collections.singletonMap("approved", true));

		waitForProcessInstanceCompleted(processInstance);

		assertThat(processInstance).hasPassedElement("dataIsValid").hasNotPassedElement("dataIsInvalid").isCompleted();

		Mockito.verify(clientRegistrationService).validateClientRegistrationData(variables);
		Mockito.verifyNoMoreInteractions(clientRegistrationService);
	}

	@Test
	public void testClientRegisteredRejected() throws Exception {
		ClientRegistrationModel variables = new ClientRegistrationModel(
				"Joao", // name
				"123456789", // contact
				"123456789", // nif
				"Portugal", // country
				"Street 123", // address
				"1234-567", // postalCode
				"joao@example.com", // email
				"password123", // password
				"EUR" // preferredCurrency
		);

		ProcessInstanceEvent processInstance = client.newCreateInstanceCommand()
				.bpmnProcessId("ClientRegistrationProcess")
				.latestVersion().variables(variables).send().join();

		waitForUserTaskAndComplete(
				"ClientRegistrationButtonForm", Collections.singletonMap("approved", true));

		waitForUserTaskAndComplete(
				"FillCustomerDataForm", Collections.singletonMap("approved", true));

		waitForProcessInstanceCompleted(processInstance);

		assertThat(processInstance).hasPassedElement("dataIsValid").isNotCompleted();

		Mockito.verify(clientRegistrationService, never()).validateClientRegistrationData(new ClientRegistrationModel(
				"", // name
				"", // contact
				"", // nif
				"", // country
				"", // address
				"", // postalCode
				"", // email
				"", // password
				"" // preferredCurrency
		));
	}
	*/
}
