package estg.ipp.pt.camundaImp.Model;

import jakarta.persistence.*;

@Entity
public class ClientRegistrationModel {
    // Attributes
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(unique = true, name = "contact", nullable = false)
    private String contact;

    @Column(unique = true, name = "nif", nullable = false)
    private String nif;

    @Column(name = "country", nullable = false)
    private String country;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(unique = true, name = "postalCode", nullable = false)
    private String postalCode;

    @Column(unique = true, name = "email", nullable = false)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(unique = true, name = "preferredCurrency", nullable = false)
    private String preferredCurrency;

    public ClientRegistrationModel() {
    }

    // Constructor
    public ClientRegistrationModel(String name, String contact, String nif, String country, String address,
            String postalCode, String email, String password, String preferredCurrency) {
        this.name = name;
        this.contact = contact;
        this.nif = nif;
        this.country = country;
        this.address = address;
        this.postalCode = postalCode;
        this.email = email;
        this.password = password;
        this.preferredCurrency = preferredCurrency;
    }

    // Getters and Setters
    public String getName() {
        return name;
    }

    public String getContact() {
        return contact;
    }

    public String getNif() {
        return nif;
    }

    public String getCountry() {
        return country;
    }

    public String getAddress() {
        return address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getPreferredCurrency() {
        return preferredCurrency;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPreferredCurrency(String preferredCurrency) {
        this.preferredCurrency = preferredCurrency;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
