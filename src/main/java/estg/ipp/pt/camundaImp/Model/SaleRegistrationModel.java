package estg.ipp.pt.camundaImp.Model;

import jakarta.persistence.*;

@Entity
public class SaleRegistrationModel {

    // Attributes

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "address")
    private String address;

    @Column(name = "selectedPaymentMethod")
    private String selectedPaymentMethod;

    @Column(name = "cardNumber")
    private String cardNumber;

    @Column(name = "expiryDate")
    private String expiryDate;

    @Column(name = "cardSecurityCode")
    private String cardSecurityCode;

    @Column(name = "paypalEmail")
    private String paypalEmail;

    @Column(name = "bankName")
    private String bankName;

    @Column(name = "bankAccountNumber")
    private String bankAccountNumber;

    @Column(name = "mbWayPhoneNumber")
    private String mbWayPhoneNumber;

    @Column(name = "iBan")
    private String iBan;

    @Column(name = "cVV")
    private String cVV;

    // Constructor
    public SaleRegistrationModel(String address, String cVV, String iBan, String selectedPaymentMethod,
            String cardNumber,
            String expiryDate,
            String cardSecurityCode, String paypalEmail, String bankName, String bankAccountNumber,
            String mbWayPhoneNumber) {
        this.address = address;
        this.cVV = cVV;
        this.iBan = iBan;
        this.selectedPaymentMethod = selectedPaymentMethod;
        this.cardNumber = cardNumber;
        this.expiryDate = expiryDate;
        this.cardSecurityCode = cardSecurityCode;
        this.paypalEmail = paypalEmail;
        this.bankName = bankName;
        this.bankAccountNumber = bankAccountNumber;
        this.mbWayPhoneNumber = mbWayPhoneNumber;
    }

    // Getters and Setters
    public String getAddress() {
        return address;
    }

    public String getSelectedPaymentMethod() {
        return selectedPaymentMethod;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public String getCardSecurityCode() {
        return cardSecurityCode;
    }

    public String getPaypalEmail() {
        return paypalEmail;
    }

    public String getBankName() {
        return bankName;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public String getMbWayPhoneNumber() {
        return mbWayPhoneNumber;
    }

    public String getiBan() {
        return iBan;
    }

    public String getcVV() {
        return cVV;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setSelectedPaymentMethod(String selectedPaymentMethod) {
        this.selectedPaymentMethod = selectedPaymentMethod;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public void setCardSecurityCode(String cardSecurityCode) {
        this.cardSecurityCode = cardSecurityCode;
    }

    public void setPaypalEmail(String paypalEmail) {
        this.paypalEmail = paypalEmail;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public void setMbWayPhoneNumber(String mbWayPhoneNumber) {
        this.mbWayPhoneNumber = mbWayPhoneNumber;
    }

    public void setiBan(String iBan) {
        this.iBan = iBan;
    }

    public void setcVV(String cVV) {
        this.cVV = cVV;
    }
}
