package estg.ipp.pt.camundaImp.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import estg.ipp.pt.camundaImp.Model.SaleRegistrationModel;

@Repository
public interface SaleRepository extends JpaRepository<SaleRegistrationModel, Long> {

}