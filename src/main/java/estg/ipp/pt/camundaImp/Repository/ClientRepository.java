package estg.ipp.pt.camundaImp.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import estg.ipp.pt.camundaImp.Model.ClientRegistrationModel;

@Repository
public interface ClientRepository extends JpaRepository<ClientRegistrationModel, Long> {

}