package estg.ipp.pt.camundaImp.JobWorker;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import estg.ipp.pt.camundaImp.Model.SaleRegistrationModel;
import estg.ipp.pt.camundaImp.Service.ISaleRegistrationService;
import io.camunda.zeebe.client.ZeebeClient;

import io.camunda.zeebe.client.api.response.ActivatedJob;
import io.camunda.zeebe.spring.client.annotation.JobWorker;

@Component
public class SaleRegistrationWorker {
    @Autowired
    private ZeebeClient client;

    @Autowired
    private ISaleRegistrationService saleRegistrationService;

    @JobWorker(type = "validateSaleData")
    public void checkSaleFormRegistrationData(final ActivatedJob job) {
        System.out.println("Checking sale registration form data");
        Map<String, Object> variables = job.getVariablesAsMap();

        /*
         * this.address = address;
         * this.cVV = cVV;
         * this.iBan = iBan;
         * this.selectedPaymentMethod = selectedPaymentMethod;
         * this.cardNumber = cardNumber;
         * this.expiryDate = expiryDate;
         * this.cardSecurityCode = cardSecurityCode;
         * this.paypalEmail = paypalEmail;
         * this.bankName = bankName;
         * this.bankAccountNumber = bankAccountNumber;
         * this.mbWayPhoneNumber = mbWayPhoneNumber;
         */
        // Convert variables to SaleRegistrationModel object
        SaleRegistrationModel saleRegistrationModel = null;
        try {
            saleRegistrationModel = new SaleRegistrationModel(
                    (String) variables.get("address"),
                    (String) variables.get("cVV"),
                    (String) variables.get("iBan"),
                    (String) variables.get("selectedPaymentMethod"),
                    (String) variables.get("cardNumber"),
                    (String) variables.get("expiryDate"),
                    (String) variables.get("cardSecurityCode"),
                    (String) variables.get("paypalEmail"),
                    (String) variables.get("bankName"),
                    (String) variables.get("bankAccountNumber"),
                    (String) variables.get("mbWayPhoneNumber"));

        } catch (Exception e) {
            System.out.println("Error converting variables to SaleRegistrationModel object");
        }

        boolean isValid = saleRegistrationService.validateSaleRegistrationData(saleRegistrationModel);

        Map<String, Object> result = Map.of("isValid", isValid);

        if (isValid) {
            // If it is valid, complete the job and inform the zeebe engine we want the
            // process to proceed through the true path
            client.newCompleteCommand(job.getKey()).variables(result).send().join();
        } else {
            // If it is not valid, complete the job and inform the zeebe engine we want the
            // process to proceed through the false path
            client.newCompleteCommand(job.getKey()).variables(result).send().join();
        }
    }

    @JobWorker(type = "registerSaleDataInDatabase")
    public void registerSaleFormRegistrationData(final ActivatedJob job) {
        System.out.println("Registering sale registration form data");
        Map<String, Object> variables = job.getVariablesAsMap();

        // Convert variables to ClientRegistrationModel object
        SaleRegistrationModel saleRegistrationModel = null;
        try {
            saleRegistrationModel = new SaleRegistrationModel(
                    (String) variables.get("address"),
                    (String) variables.get("cVV"),
                    (String) variables.get("iBan"),
                    (String) variables.get("selectedPaymentMethod"),
                    (String) variables.get("cardNumber"),
                    (String) variables.get("expiryDate"),
                    (String) variables.get("cardSecurityCode"),
                    (String) variables.get("paypalEmail"),
                    (String) variables.get("bankName"),
                    (String) variables.get("bankAccountNumber"),
                    (String) variables.get("mbWayPhoneNumber"));
        } catch (Exception e) {
            System.out.println("Error converting variables to SaleRegistrationModel object");
        }

        saleRegistrationService.registerSaleRegistrationData(saleRegistrationModel);

        Map<String, Object> result = Map.of("saleRegistrationModel", saleRegistrationModel);

        client.newCompleteCommand(job.getKey()).variables(result).send().join();
    }
}