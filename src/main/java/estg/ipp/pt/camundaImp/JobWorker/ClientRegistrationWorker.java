package estg.ipp.pt.camundaImp.JobWorker;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import estg.ipp.pt.camundaImp.Model.ClientRegistrationModel;
import estg.ipp.pt.camundaImp.Service.IClientRegistrationService;
import io.camunda.zeebe.client.ZeebeClient;
import io.camunda.zeebe.client.api.response.ActivatedJob;
import io.camunda.zeebe.spring.client.annotation.JobWorker;

@Component
public class ClientRegistrationWorker {

    // private ClientRegistrationHandler clientRegistrationHandler;
    @Autowired
    private ZeebeClient client;

    @Autowired
    private IClientRegistrationService clientRegistrationService;

    @JobWorker(type = "validateClientData")
    public void checkClientFormRegistrationData(final ActivatedJob job) {
        System.out.println("Checking client registration form data");
        Map<String, Object> variables = job.getVariablesAsMap();

        variables.forEach((key, value) -> {
            System.out.println(key + ":" + value);
        });

        // Convert variables to ClientRegistrationModel object
        ClientRegistrationModel clientRegistrationModel = null;
        try {
            clientRegistrationModel = new ClientRegistrationModel(
                    variables.get("name").toString(),
                    variables.get("contact").toString(),
                    variables.get("nif").toString(),
                    variables.get("country").toString(),
                    variables.get("address").toString(),
                    variables.get("postalCode").toString(),
                    variables.get("email").toString(),
                    variables.get("password").toString(),
                    variables.get("preferredCurrency").toString());
        } catch (Exception e) {
            e.printStackTrace(); // Example of logging to console
            System.out.println("Error converting variables to ClientRegistrationModel object");
        }

        boolean isValid = clientRegistrationService.validateClientRegistrationData(clientRegistrationModel);

        Map<String, Object> result = Map.of("isValid", isValid);

        if (isValid) {
            // If it is valid, complete the job and inform the zeebe engine we want the
            // process to proceed through the true path
            client.newCompleteCommand(job.getKey()).variables(result).send().join();
        } else {
            // If it is not valid, complete the job and inform the zeebe engine we want the
            // process to proceed through the false path
            client.newCompleteCommand(job.getKey()).variables(result).send().join();
        }
    }

    @JobWorker(type = "registerClientDataInDatabase")
    public void registerClientFormRegistrationData(final ActivatedJob job) {
        System.out.println("Registering client registration form data");
        Map<String, Object> variables = job.getVariablesAsMap();

        // Convert variables to ClientRegistrationModel object
        ClientRegistrationModel clientRegistrationModel = null;
        try {
            clientRegistrationModel = new ClientRegistrationModel(
                    variables.get("name").toString(),
                    variables.get("contact").toString(),
                    variables.get("nif").toString(),
                    variables.get("country").toString(),
                    variables.get("address").toString(),
                    variables.get("postalCode").toString(),
                    variables.get("email").toString(),
                    variables.get("password").toString(),
                    variables.get("preferredCurrency").toString());
        } catch (Exception e) {
            e.printStackTrace(); // Example of logging to console
            System.out.println("Error converting variables to ClientRegistrationModel object");
        }

        clientRegistrationService.registerClientRegistrationData(clientRegistrationModel);

        Map<String, Object> result = Map.of("clientRegistrationModel", clientRegistrationModel);

        client.newCompleteCommand(job.getKey()).variables(result).send().join();
    }
}