package estg.ipp.pt.camundaImp.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import estg.ipp.pt.camundaImp.Model.ClientRegistrationModel;
import estg.ipp.pt.camundaImp.Model.SaleRegistrationModel;
import estg.ipp.pt.camundaImp.Repository.SaleRepository;

@Service
public class SaleRegistrationService implements ISaleRegistrationService {

    private final SaleRepository saleRepository;

    @Autowired
    public SaleRegistrationService(SaleRepository saleRepository) {
        this.saleRepository = saleRepository;
    }

    @Override
    public boolean validateSaleRegistrationData(SaleRegistrationModel saleRegistrationModel) {
        if (saleRegistrationModel == null) {
            return false;
        }

        if (saleRegistrationModel.getAddress().isEmpty() ||
                saleRegistrationModel.getSelectedPaymentMethod().isEmpty()) {
            return false;
        }

        return true;
    }

    @Override
    public SaleRegistrationModel registerSaleRegistrationData(SaleRegistrationModel saleRegistrationModel) {
        return this.saleRepository.saveAndFlush(saleRegistrationModel);
    }

    public List<SaleRegistrationModel> getAllSales() {
        return this.saleRepository.findAll();
    }
}