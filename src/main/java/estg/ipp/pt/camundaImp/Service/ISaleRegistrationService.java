package estg.ipp.pt.camundaImp.Service;

import estg.ipp.pt.camundaImp.Model.SaleRegistrationModel;

public interface ISaleRegistrationService {
    public boolean validateSaleRegistrationData(SaleRegistrationModel saleRegistrationModel);

    public SaleRegistrationModel registerSaleRegistrationData(SaleRegistrationModel saleRegistrationModel);
}