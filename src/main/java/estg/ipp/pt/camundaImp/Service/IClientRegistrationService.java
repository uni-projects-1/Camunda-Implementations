package estg.ipp.pt.camundaImp.Service;

import estg.ipp.pt.camundaImp.Model.ClientRegistrationModel;

public interface IClientRegistrationService {
    public boolean validateClientRegistrationData(ClientRegistrationModel clientRegistrationModel);

    public ClientRegistrationModel registerClientRegistrationData(ClientRegistrationModel clientRegistrationModel);
}