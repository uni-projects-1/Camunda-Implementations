package estg.ipp.pt.camundaImp.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import estg.ipp.pt.camundaImp.Model.ClientRegistrationModel;
import estg.ipp.pt.camundaImp.Repository.ClientRepository;

import java.util.List;

@Service
public class ClientRegistrationService implements IClientRegistrationService {

    private final ClientRepository clientRepository;

    @Autowired
    public ClientRegistrationService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public boolean validateClientRegistrationData(ClientRegistrationModel clientRegistrationModel) {
        System.out.println("Validating client registration data");
        if (clientRegistrationModel == null) {
            return false;
        }

        if (clientRegistrationModel.getName().isEmpty() ||
                clientRegistrationModel.getContact().isEmpty() ||
                clientRegistrationModel.getNif().isEmpty() ||
                clientRegistrationModel.getCountry().isEmpty() ||
                clientRegistrationModel.getAddress().isEmpty() ||
                clientRegistrationModel.getPostalCode().isEmpty() ||
                clientRegistrationModel.getEmail().isEmpty() ||
                clientRegistrationModel.getPassword().isEmpty() ||
                clientRegistrationModel.getPreferredCurrency().isEmpty()) {
            return false;
        }

        return true;
    }

    @Override
    public ClientRegistrationModel registerClientRegistrationData(ClientRegistrationModel clientRegistrationModel) {
        System.out.println("Registering client registration data");
        return this.clientRepository.saveAndFlush(clientRegistrationModel);
    }

    public List<ClientRegistrationModel> getAllClients() {
        return this.clientRepository.findAll();
    }
}
