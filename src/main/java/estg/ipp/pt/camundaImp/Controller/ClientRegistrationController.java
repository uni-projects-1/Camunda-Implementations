package estg.ipp.pt.camundaImp.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import estg.ipp.pt.camundaImp.Model.ClientRegistrationModel;
import estg.ipp.pt.camundaImp.Service.ClientRegistrationService;

import java.util.List;

@RestController
@RequestMapping("/api/client")
public class ClientRegistrationController {

    @Autowired
    private final ClientRegistrationService clientRegistrationService;

    public ClientRegistrationController(ClientRegistrationService clientRegistrationService) {
        this.clientRegistrationService = clientRegistrationService;
    }

    @GetMapping("/hello")
    public String hello() {
        return "Hello World!";
    }

    @PostMapping("/registerClient")
    public void registerClient(@RequestBody ClientRegistrationModel clientRegistrationDto) {
        System.out.println("Registering client registration data");
        clientRegistrationService.registerClientRegistrationData(clientRegistrationDto);
    }

    @PostMapping("/validateData")
    public Boolean validateData(@RequestBody ClientRegistrationModel clientRegistrationDto) {
        System.out.println("Validating client registration data");
        return clientRegistrationService.validateClientRegistrationData(clientRegistrationDto);
    }

    @GetMapping("")
    public List<ClientRegistrationModel> getClients() {
        return clientRegistrationService.getAllClients();
    }
}
