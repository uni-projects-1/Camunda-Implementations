package estg.ipp.pt.camundaImp.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import estg.ipp.pt.camundaImp.Model.ClientRegistrationModel;
import estg.ipp.pt.camundaImp.Model.SaleRegistrationModel;
import estg.ipp.pt.camundaImp.Service.SaleRegistrationService;

@RestController
@RequestMapping("/api/sale")
public class SaleRegistrationController {

    @Autowired
    private final SaleRegistrationService saleRegistrationService;

    public SaleRegistrationController(SaleRegistrationService saleRegistrationService) {
        this.saleRegistrationService = saleRegistrationService;
    }

    @PostMapping("/registerSale")
    public void registerClient(@RequestBody SaleRegistrationModel clientRegistrationDto) {
        saleRegistrationService.registerSaleRegistrationData(clientRegistrationDto);
    }

    @PostMapping("/validateData")
    public Boolean validateData(@RequestBody SaleRegistrationModel clientRegistrationDto) {
        return saleRegistrationService.validateSaleRegistrationData(clientRegistrationDto);
    }

    @GetMapping("")
    public List<SaleRegistrationModel> getSales() {
        return saleRegistrationService.getAllSales();
    }
}