package estg.ipp.pt.camundaImp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "estg.ipp.pt.camundaImp" })
public class CamundaImpApplication {
	public static void main(String[] args) {
		SpringApplication.run(CamundaImpApplication.class, args);
	}
}
