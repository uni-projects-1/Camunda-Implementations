# Camunda-Implementations
## Getting started
To start the project, update the application.properties file to host your cluster's client properties

Afterwards, create an instance (in Camunda SaaS 8) for the process of a diagram similar to the ones in the folder resources

In the terminal, run `gradle bootRun` (you need Gradle to run the project)

If you make any service tasks with the same type as the ones defined in the JobWorker folder, the code inside the job worker that was triggered will start running

